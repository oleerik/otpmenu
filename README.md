otpmenu is a fork of passmenu, which is a part of pass.
The point of this fork is to make OTP codes easily accessable through the use of pass and the pass-otp extension.

Requirements:
* Pass
* Pass-OTP
* dmenu
* zenity 
* otp.gpg files (gitlab/otp.gpg)

An easy way to adding the otp codes properly:
pass otp insert totp -s "THISISASECRET" gitlab/otp